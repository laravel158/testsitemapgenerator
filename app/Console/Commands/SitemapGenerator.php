<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Smg\SitemapGenerator\Services\Sitemap;

class SitemapGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '
        sitemap:generator
        {--format=xml : тип файла для генерации. Допустимые значения: xml, json или csv}
        {--path=./storage/sitemap/sitemap.xml : путь к файлу для сохранения}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Библиотека генерации карты сайта в различных файловых форматах: xml, csv, json';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listPages = [
            [
                'loc' => 'https://site.ru',
                'lastmod' => '2020-12-14',
                'priority' => 0,
                'changefreq' => 'hourly'
            ],
            [
                'loc' => 'https://site.ru/news',
                'lastmod' => '2020-12-10',
                'priority' => 0.5,
                'changefreq' => 'daily'
            ],
            [
                'loc' => 'https://site.ru/about',
                'lastmod' => '2020-12-01',
                'priority' => 0.2,
                'changefreq' => 'weekly'
            ],
            [
                'loc' => 'https://site.ru/contact',
                'lastmod' => '2020-12-21',
                'priority' => 0.8,
                'changefreq' => 'monthly'
            ],
            [
                'loc' => 'https://site.ru/show',
                'lastmod' => '2022-09-02',
                'priority' => 0.8,
                'changefreq' => 'monthly'
            ],
            [
                'loc' => 'https://site.ru/api/app',
                'lastmod' => '2022-09-02',
                'priority' => 0.8,
                'changefreq' => 'monthly'
            ],
        ];

        try {
            $sitemap = new Sitemap($listPages, $this->option('format'), $this->option('path'));

            $sitemap->build();
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }

        return 0;
    }
}
