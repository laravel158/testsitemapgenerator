### Проект для проверки ***Библиотеки генерации карты сайта***

- [Packagist smg/sitemap-generator](https://packagist.org/packages/smg/sitemap-generator)
- [Gitlab sitemap-generator](https://gitlab.com/ivanpopko/sitemap-generator)

#### Описание
Библиотека генерации карты сайта в различных файловых форматах: xml, csv, json

При инициализации библиотеки в скрипте передается **список страниц сайта в виде массива с параметрами**: адрес страницы (loc), дата изменения страницы (lastmod), приоритет парсинга (priority), периодичность обновления (changefreq). 
Также при инициализации передается **тип файла** для генерации: xml, csv, json; и **путь к файлу для сохранения**.

После инициализации объект библиотеки формирует файл выбранного типа карты сайта.


### Установка
1. Склонировать проект **testsitemapgenerator**: `git clone git@gitlab.com:laravel158/testsitemapgenerator.git`
2. Перейти в проект: `cd testsitemapgenerator`
3. Установить Composer зависимости: `php composer install`
4. Создать ***.env***: `cp .env.example .env`
5. Сгенерировать ключ: `php artisan key:generate`
6. Запустить генерацию карты сайта: `php artisan sitemap:generator`

> Команду ``sitemap:generator`` возможно запустить с параметрами:
>- **format** - тип файла для генерации. Допустимые значения: xml, json или csv
>- **path** - путь к файлу для сохранения.

##### Примеры запуска:
`php artisan sitemap:generator`

`php artisan sitemap:generator --format=xml --path=./storage/sitemap/sitemap.xml`

`php artisan sitemap:generator --format=json --path=./storage/sitemap/sitemap.json`

`php artisan sitemap:generator --format=csv --path=./storage/sitemap/sitemap.csv`


##### Описание команды
`php artisan --help sitemap:generator 
